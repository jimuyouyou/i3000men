var qiteSites = [
  {
    free: true,
    name: '最后的登月任务',
    link: 'https://apollo17.org/',
    tags: '模拟,阿波罗,登月',
    desc: '在这个网站里，拥有超过300小时的音频、超过22小时的视频以及超过 3600 张照片，甚至你可以模拟阿波罗17号登月的全过程，重温1972年发生的每一刻。'
  },
  {
    free: true,
    name: '飞机坠毁信息',
    link: 'http://www.planecrashinfo.com/',
    tags: '空难,事故',
    desc: '历史上发生空难以来的事件，包括事故照片、事故地图等等.'
  },
  {
    free: true,
    name: 'Earth 地球',
    link: 'https://earth.nullschool.net/',
    tags: '全球,气象,海洋,污染',
    desc: '这是一款观察全球气象，海洋的网站。据说P社玩家人均地图，点击earth你可以查看大气，海洋，化学污染物，气象变化等...'
  },
  {
    free: true,
    name: 'LAND LINES',
    link: 'https://lines.chromeexperiments.com/',
    tags: '地球,风景,画线,指点',
    desc: '是不是有一种指点江山的感觉'
  },
  {
    free: true,
    name: '最后的登月任务',
    link: 'https://apollo17.org/',
    tags: '模拟,阿波罗,登月',
    desc: '在这个网站里，拥有超过300小时的音频、超过22小时的视频以及超过 3600 张照片，甚至你可以模拟阿波罗17号登月的全过程，重温1972年发生的每一刻。'
  },

];