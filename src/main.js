function showSites(sites) {
  var doorStr = '';
  for (let i = 0; i < sites.length; i++) {
    var site = sites[i];
    var tagClass = 'tags ' + (site.free ? 'free' : '');
    doorStr += '<div class="oneSite"><a target="__blank" href="' + site.link + '">' + site.name + '</a>';
    doorStr += '<span class="' + tagClass + '">' + site.tags + '</span>';
    doorStr += '<div class="desc">' + site.desc + '</div></div>';
  }
  document.querySelector('.door-wrapper').innerHTML = doorStr;
}

function searchSites(key) {
  var fenci = new JsFenci();
  fenci.init(keywords);
  var ks = fenci.splitWords(key);

  var sites = [];
  if (ks && ks.length > 0) {
    for (var i = 0; i < allSites.length; i++) {
      var site = allSites[i];
      var matchCount = 0;
      for (var p = 0; p < ks.length; p++) {
        if (site.tags.includes(ks[p])) {
          matchCount++;
        }
      }
      if (matchCount) {
        var st = _.clone(site);
        st.matchCount = matchCount;
        sites.push(st);
      }
    }
  }


  var sortedSites = (sites.length > 0) ? _.slice(_.orderBy(sites, 'matchCount', 'desc'), 0, 5) : sites;
  showSites(sortedSites);
}

function openDoor() {
  var key = $('#searchInput').val().trim().toUpperCase();
  if (key && key.length > 0) {
    searchSites(key);
    $('#bingIframe').attr('src', 'https://cn.bing.com/search?q=' + key);
  } else {
    randomDoor();
    $('#bingIframe').attr('src', '');
  }
}

function randomDoor() {
  var arrIndex = getRandomNumbers(5, 0, allSites.length - 1);
  var sites = [];
  for (var i = 0; i < arrIndex.length; i++) {
    sites.push(allSites[arrIndex[i]]);
  }
  showSites(sites);
}



// document init
var allSites = [];
$(document).ready(function () {
  allSites = youyongSites.concat(qiteSites);

  randomDoor();

  $('#searchInput').focus();

  $("#openDoor").click(function () {
    openDoor();
  });

  $("#searchInput").keydown(function (e) {
    var e = e || event,
      keycode = e.which || e.keyCode;
    if (keycode == 13) {
      openDoor();
    }
  });

  $("#toTop").click(function () {
    $('#searchInput').focus();
  });


});

