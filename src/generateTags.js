const fs = require('fs');

const yy = fs.readFileSync('src/youyongSites.js');
const yySites = eval(yy.toString().replace('var youyongSites =', ''));

const qt = fs.readFileSync('src/qiteSites.js');
const qtSites = eval(qt.toString().replace('var qiteSites =', ''));

const allSites = yySites.concat(qtSites);
const tags = new Set();
allSites.forEach(s => {
  if (s) {
    s.tags.split(',').forEach(st => {
      if (st.trim()) {
        tags.add(st.trim());
      }
    });
  }
});

const tagsArray = Array.from(tags.values());
const data = `var keywords = ${JSON.stringify(tagsArray)};`;
fs.writeFileSync('src/keywords.js', data);


