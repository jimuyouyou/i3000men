var youyongSites = [
  {
    free: true,
    name: 'PPT 超级市场',
    link: 'http://ppt.sotary.com/web/wxapp/index.html',
    tags: 'PPT,优质,高效,安全',
    desc: '每一个PPT模板的质量都是极高，并且非常精美。它的界面非常简洁，没有任何多余的东西，只有一个简单的搜索框和部分推荐模板，你可以直接利用网站的搜索功能搜索你需要的PPT模板。'
  },
  {
    free: true,
    name: '熊猫搜书',
    link: 'https://ebook.huzerui.com/',
    tags: '电子书,优质,导航,搜索',
    desc: '它聚合了多达 20 多个高质量电子书网站，并且每个网站的质量都是非常高，它的界面简洁清新，你可以直接点击左侧切换网站。'
  },
  {
    free: false,
    name: '网易见外',
    link: 'https://jianwai.netease.com',
    tags: '多功能,黑科技',
    desc: '它支持每日 2 小时免费体验.，它支持的黑科技实用功能包括：视频翻译、视频转写、字幕翻译、文档翻译、语音翻译、语音转写、会议同传、图片翻译，每个功能都是非常实用。'
  },
  {
    free: true,
    name: '图形方格纸',
    link: 'https://www.mygraphpaper.com/index.php%3Flang%3Dzh-hans',
    tags: '免费,纸张,生成,精美',
    desc: '它提供的纸张类别包括：笔记本、东大笔记本、康奈尔笔记、方格纸、方格图、网格纸、备忘录、漫画稿纸、五线谱、TAB 吉他谱、自填周记、学习二分割、行动的基础。'
  },
  {
    free: true,
    name: '秘塔写作猫',
    link: 'https://xiezuocat.com/',
    tags: '智能,写作,错误,提示,全文,改写',
    desc: '不可多得的 AI 智能写作工具，它专注于中文写作，功能非常强大，每个功能都可以帮助你更好的进行中文写作。'
  },

  {
    free: true,
    name: '联图云光盘',
    link: 'http://discx.yuntu.io/',
    tags: '宝藏,书籍,光盘,播放,',
    desc: '宝藏书籍光盘网站，也是一个非常好用的在线学习网站。'
  },
  {
    free: true,
    name: 'Fosshub',
    link: 'https://www.fosshub.com/',
    tags: '开源,电脑,软件,良心',
    desc: '提供完全免费开源电脑软件的良心网站，它专注于分享完全免费并且开源的电脑软件，上面的电脑软件完全免费开源，并且没广告软件，有恶意软件，没有间谍软件，没有捆绑包。'
  },
  {
    free: true,
    name: '幕享',
    link: 'https://letsview.com/',
    tags: '画质,高清,流畅,不卡顿,投屏',
    desc: '良心并且好用的无线投屏工具，它完全免费，支持全平台投屏，支持将你的安卓手机和苹果手机投屏到电脑上；你可以下载包括：电脑桌面端、安卓端、 IOS 端。'
  },
  {
    free: true,
    name: 'Maspeak',
    link: 'https://maspeak.com/',
    tags: '外语,学习,图片,趣味性',
    desc: '让你趣味性学习多种语言单词的实用网站，它支持学习的语言包括：法语、英语、西班牙语、意大利语、德语、阿拉语、俄语、韩语、日语。'
  },
  {
    free: true,
    name: '凡科快图',
    link: 'https://kt.fkw.com/muban.html',
    tags: '海报,制作,图片,编辑,AI,抠图',
    desc: '所有图片海报模板是免费的，所有图片处理工具是免费的，抠图功能也是免费的，你只需要注册登录就可以全部免费使用。它免费提供了丰富的图片海报模板，你只需要三步，就可以一键生成精美的图片海报。'
  },

  {
    free: true,
    name: 'GitMind',
    link: 'https://gitmind.cn/app/template?lang=zh',
    tags: '思维导图,流程图,导出,多人,协同',
    desc: '完全免费的在线思维导图制作网站。它整体操作界面简洁清晰，让你操作起来非常轻松；它支持插入图片、链接、备注，并且提供丰富的布局样式。'
  },
  {
    free: true,
    name: 'Oalib',
    link: 'http://www.oalib.com/',
    tags: '论文,免费,下载,搜索',
    desc: '提供超过 420 万篇论文，并且所有文章支持免费下载，所有文章支持以 PDF 格式下载；它支持强大的搜索功能，你可以利用网站的高级搜索功能进一步搜索你需要的文章。'
  },
  {
    free: true,
    name: 'City roads',
    link: 'https://anvaka.github.io/city-roads/',
    tags: '地图,生成,全球,城市,路网,地图',
    desc: '让人惊艳的路网地图生成网站，它可以一键生成全球任意城市的路网地图，你可以修改路网地图的背景颜色和线条颜色，生成多张不同风格的路网地图。'
  },
  {
    free: true,
    name: '地图生成器',
    link: 'http://datav.aliyun.com/tools/atlas',
    tags: '编辑,下载,地图,生成',
    desc: '可以下载到各省，各市，各县的SVG格式的地图素材.这些素材导入PPT中都是可以编辑的。可以单独更改颜色和轮廓。'
  },
  {
    free: true,
    name: 'Pexels',
    link: 'https://www.pexels.com/',
    tags: '图片,优质,版权',
    desc: '高质量的图片网站，资源非常丰富，免费的，无版权的'
  },


  {
    free: true,
    name: 'Iconfont',
    link: 'https://www.iconfont.cn/',
    tags: '矢量,图库',
    desc: '阿里巴巴体验团队倾力打造的中国第一个最大且功能最全的矢量图标库，有上千万个图标。你想要的图标，这里基本上都可以找到。'
  },
  {
    free: true,
    name: '觅元素',
    link: 'http://www.51yuansu.com/',
    tags: '图片,高清,PNG,免抠,设计',
    desc: '专注做高清PNG免抠设计的网站。图片可以免费下载。'
  },
  {
    free: true,
    name: '去背景',
    link: 'https://www.remove.bg/',
    tags: '抠图,AI,效果好',
    desc: '在线抠图的神器，只需要上传图片，就可以智能抠图，而且抠图的效果非常好。'
  },
  {
    free: true,
    name: '站酷网',
    link: 'https://www.zcool.com.cn/',
    tags: '设计,酷,灵感',
    desc: '很酷的设计网站，有很多国潮元素和设计大拿，是一个获取灵感的好地方。'
  },
  {
    free: true,
    name: '空投',
    link: 'https://airportal.cn/',
    tags: '即时,文件,分享',
    desc: '免费即时文件分享网站，界面简洁素雅'
  },

  {
    free: true,
    name: '稿定抠图',
    link: 'https://www.gaoding.com/koutu',
    tags: '背景,抠图,证件照,批量,抠商品',
    desc: '抠物品只要两笔，选择想要和不想要的部分，其余交给我，不出一分钟就能得到一张高清无底的透明png图片。还支持批量抠图，一次上传几十张图什么的，都是小意思。'
  },
  {
    free: true,
    name: 'docsmall',
    link: 'https://docsmall.com/',
    tags: '文件,压缩,图片,GIF,PDF,合并,分割',
    desc: 'docsmall是一个免费在线文件压缩网站，只需上传、处理、下载简单三步即可完成操作，目前该网站支持图片压缩、GIF压缩、PDF压缩、PDF合并、PDF分割功能，比如在知乎发原创时，对图片大小有要求，那么可以通过这个网站将图片压缩好。'
  },
  {
    free: true,
    name: 'Flourish',
    link: 'https://flourish.studio/',
    tags: '可视化,数据,Excel',
    desc: '一个非常好用的在线制作可视化数据的网站，不需要懂得代码就可以让我们的数据动起来，更加直观地展示我们的数据。它是一个在线制作的网站工具，不管是电脑还是手机/平板，只要操作者把数据文件（例如我使用的是Excel文件）上传到该平台就可以轻松完成可视化的工作。'
  },
  {
    free: true,
    name: '白描网页版',
    link: 'https://web.baimiaoapp.com/',
    tags: '图片,文字,提取,表格,识别,扫描,PDF,转文字',
    desc: '它提供了很简单好用的三个功能：图片文字提取、电子表格识别、扫描PDF转文字。都是实用功能，关键是免费使用。'
  },
  {
    free: true,
    name: '微软爱写作',
    link: 'http://aimwriting.mtutor.engkoo.com/',
    tags: 'AI,英文,作文,打分,修改',
    desc: '巨头微软推出的一款免费工具，利用AI对英文作文进行打分和修改的网站。覆盖8种考试类型，用AI自动检查语法和拼写错误，并给出修改意见，可以拍照上传自动识别。从小学到托福、雅思，提供了一下8类考试内容。'
  },

  {
    free: true,
    name: '中国色',
    link: 'http://zhongguose.com/',
    tags: '色彩,搭配,中式,RGB',
    desc: '没错，这是一个色彩搭配网站，不过注重点跟名字一样，重点收集中国风味的颜色，颜色种类十分丰富，嗯~是满满的中国风，点击RGB搭配的右侧就可以复制、导入颜色了，十分的方便。'
  },
  {
    free: true,
    name: '炫光生成器',
    link: 'http://weavesilk.com/',
    tags: '酷,玄',
    desc: '一个超炫酷的镜像玄关生成网站，设计师必备。'
  },
  {
    free: true,
    name: '万能命令生成器',
    link: 'https://wanneng.run/cn/',
    tags: '万能,命令,工具,下载,截图,PDF,脚本,油猴,视频,电影,在线',
    desc: '它会根据不同的网站，提供不同的在线工具！'
  },
  {
    free: true,
    name: 'world68',
    link: 'http://www.world68.com/',
    tags: '好用,世界,神器,大全',
    desc: '网站有点简陋，却收集了全世界最厉害的网站，涵盖各个方面，绝对的神器！网站按照不同得导航分类，让你轻松可以找到想看得国家网站!'
  },
  {
    free: true,
    name: '阅后即焚生成器',
    link: 'https://sesme.co/',
    tags: '表白,有趣',
    desc: '网站不需要注册，打开就能直接用。支持设置1-99秒的消息展示时长'
  },


  {
    free: true,
    name: '无用网',
    link: 'https://theuselessweb.com/',
    tags: '小众,魔性,有趣,无聊,无用,好玩',
    desc: '每次点击，都会把你带到一个陌生的网站，各种各样奇怪的网站'
  },
  {
    free: true,
    name: '虫部落',
    link: 'http://www.chongbuluo.cn/',
    tags: '电子书,下载,小众',
    desc: '收藏这一个网站就够了！毫不客气的讲，一个网站顶10个电子书网站！'
  },
  {
    free: true,
    name: '创意举牌小人生成器',
    link: 'https://upuptoyou.com/',
    tags: '创意,举牌,小人',
    desc: '一个可以生成各种各样举牌小人的网站，非常有创意！'
  },
  {
    free: true,
    name: '初音未来生成器',
    link: 'https://aidn.jp/mikutap/',
    tags: '质感,音乐,生成',
    desc: '只需要点击鼠标，或者敲打键盘，初音的声音便会生成，根据你敲打的频次等，声音发生变化，最后生成你自己专属的初音音乐！'
  },
  {
    free: true,
    name: '字幕酱',
    link: 'https://www.zimujiang.com/',
    tags: 'AI,生成,字幕,多语言',
    desc: '基于AI快速生成视频、音频字幕信息，1分钟内的短视频免费，为UP主节省大量字幕打轴时间。基于AI人工智能，在线制作双语字幕，支持中文、英文、日语、韩语、德语、法语等语种，自动语音转字幕。'
  },


  {
    free: true,
    name: '迅捷画图',
    link: 'https://www.liuchengtu.com/',
    tags: '流程图,思维导图,模板',
    desc: '支持流程图、思维导图、组织结构、ER图、网络拓扑图、UML图，一个网站解决你的办公画图难题！'
  },
  {
    free: true,
    name: '内容神器',
    link: 'https://www.5ce.com/',
    tags: '新媒体,神器,运营,热点',
    desc: '当中备受追捧的智能编辑器，大数据热点分析判断哪些热点值得追，自动找素材提高创作灵感，样式排版让公众号文章更美观，最为青睐的各项AI功能智能检测，不仅提高收录率，让文章更有保障。'
  },
  {
    free: true,
    name: '超级简历',
    link: 'https://www.wondercv.com/',
    tags: '简历,生成,专业',
    desc: '想做一份专业，简捷的简历，可以用这个网站制作，主要把你需要的内容输入进去，排版什么的就交给网站吧，而且可以根据自己的岗位，职业进行定制化，不怕与别人的简历产生雷同，写完之后，网站的AI可以根据你写的简历提出优化建议，比如工作经历，项目总结等怎样写会更完善。推荐给大家。'
  },
  {
    free: true,
    name: 'Grammarly',
    link: 'https://www.grammarly.com/',
    tags: '英文,纠错,写作,智能',
    desc: '英文写作智能纠错，免费的写作助手。也可以添加到浏览器插件，然后你在任何网站输入框里输入文本，都会自动给你纠错。'
  },
  {
    free: true,
    name: 'DeepL翻译',
    link: 'https://www.deepl.com/translator',
    tags: '翻译,多语言,中译英,英译中,最强',
    desc: '用神经网络技术为您的文本提供最好的机器翻译，很多人更喜欢用谷歌翻译，但是这家德国公司出的产品貌似有比谷歌更惊人的翻译能力。'
  },


  {
    free: true,
    name: '妙笔',
    link: 'https://www.atominn.com/wonderpen',
    tags: '翻译,多语言,中译英,英译中,最强',
    desc: '用神经网络技术为您的文本提供最好的机器翻译，很多人更喜欢用谷歌翻译，但是这家德国公司出的产品貌似有比谷歌更惊人的翻译能力。'
  },
  {
    free: true,
    name: 'LingVist',
    link: 'https://lingvist.com/',
    tags: 'AI,学英语,外语',
    desc: 'LingVist   是一位从小就讨厌学习外语的人创办的语言学习服务平台，主打快速、聪明且高效的学习，利用大数据统计生活中最常用的英语单词，结合人工智能学习，从英语菜鸟，到看懂英语视频。'
  },
  {
    free: true,
    name: 'DeepAngel',
    link: 'http://deepangel.media.mit.edu/',
    tags: '智能,抠图,',
    desc: 'DeepAngel是一款基于一种人工智能AI的抠图工具，可以帮助用户把图片中的物体删除，这款工具有美国麻省大学发布，其中融入了部分艺术、部分技术和部分哲学，抠图也可以AI了。'
  },
  {
    free: true,
    name: 'FotoForensics',
    link: 'http://fotoforensics.com/',
    tags: 'PS,检测,修图',
    desc: 'FotoForensics 轻松帮你检测判定一张照片是否被PS处理修改过，P过的照骗通通无所遁形。'
  },
  {
    free: true,
    name: '天空之城',
    link: 'https://www.skypixel.com/',
    tags: '航拍,摄影,图片',
    desc: '这个天空之城不是宫崎骏的动画电影，也不是韩剧，而是一个全球作品的社区，里面都是一些签约摄影师上传的静态动态摄影作品。'
  },

  {
    free: true,
    name: '街头艺术资源网',
    link: 'https://www.streetartutopia.com/',
    tags: '街头,艺术,收藏,创意',
    desc: '乌托邦平台，拥有众多的艺术家和收藏作品，可以点击网站的任意链接，查找相关的艺术家作品和主题作品。'
  },
  {
    free: true,
    name: 'Colorssal',
    link: 'https://www.thisiscolossal.com/',
    tags: '图片,视频,有趣,创意,素材',
    desc: '里面也提供了一些很有趣的视频和创意图片，它们也是很多账号素材的宠儿，翻了翻发现了很多爆款视在这里也被收录了'
  },
  {
    free: false,
    name: '8kraw',
    link: 'https://www.8kraw.com/',
    tags: '航拍,摄影,图片',
    desc: '很多值得看的图片网站，大片背后也是无数摄影师的无数次攀登与摄影，大家如果要商用最好还是咨询一下。'
  },
  {
    free: true,
    name: '随机渐变配色',
    link: 'https://uigradients.com/#Christmas',
    tags: '渐变,配色',
    desc: '如果你只想要某个颜色的渐变色组合，可以在右上角点击按钮，然后选择对应的颜色，这时候切换的便是你选择的颜色渐变。‍‍'
  },
  {
    free: true,
    name: '日本色',
    link: 'https://nipponcolors.com/#matsuba',
    tags: '配色,日本',
    desc: '这是一个第一次见会被惊艳到，但实际上用起来可能没这么好的配色网站，整个界面排版非常舒服，'
  },

  {
    free: true,
    name: 'dribbble',
    link: 'https://dribbble.com/',
    tags: '设计,冲浪,素材,创意,图片',
    desc: 'Dribbble，也是一个适合设计师冲浪的地方，里面也有很多的设计作品和素材可以浏览，用来收集优秀设计还是可以的！'
  },
  {
    free: true,
    name: '果汁排行榜',
    link: 'http://www.guozhivip.com/rank/',
    tags: '榜单,热搜,排行,话题',
    desc: '如果你在网上冲浪，想同时看很多个榜单的热搜，在这里推荐果汁排行榜，种类繁多，包括平台话题、影视榜、音乐、产品、财富榜等等。'
  },
  {
    free: true,
    name: '今日热榜',
    link: 'https://tophub.today/',
    tags: '榜单,热搜,排行,话题',
    desc: '当然，以前被我们安利过的今日热榜，更加侧重平台的热榜，且呈现页面会与果汁排行榜不一样，会直接在主页上显示实时的话题排行。</p>'
  },
  {
    free: true,
    name: 'Oeasy',
    link: 'http://oeasy.org/',
    tags: '视频,自学',
    desc: '这是一个对小白很友好的视频自学网站。它的特点是：方便、免费、高效率'
  },
  {
    free: true,
    name: '考试酷',
    link: 'https://www.examcoo.com/index/ku',
    tags: '题库,海量',
    desc: '如果你是正在准备考试的学生党，可以用这个网站快速完成自我检测。'
  },

  {
    free: true,
    name: 'wikiHow',
    link: 'https://zh.wikihow.com/',
    tags: '成人,百科,接地气,专业,干货',
    desc: 'wikihow之所以强大，在于用专业知识，帮你解决一些看似不正经，却很接地气的问题。'
  },
  {
    free: true,
    name: 'CrashCourse',
    link: 'https://crashcourse.club/category/',
    tags: '中文,字幕,自学,视频',
    desc: '虽然不是原创视频，但都是外网广受好评的高质量资源。字幕组不仅给它们添加了清晰、准确的字幕，还分好类别，以及更新进度。'
  },
  {
    free: true,
    name: '知妖',
    link: 'https://www.cbaigui.com/',
    tags: '妖怪,百科',
    desc: '知妖，一部中国妖怪百科全书。一个网站，让你认识中国从古至今各种妖怪。'
  },
  {
    free: true,
    name: 'Noize',
    link: 'https://noize.ml/',
    tags: '放松,焦虑,休闲,专注,失眠',
    desc: '平稳的白噪音，不仅能让你深度放松，有效缓解焦虑、失眠，还能让你进入深度专注。所以睡不着，或者学不进去时，打开Noize，准有奇效。'
  },
  {
    free: true,
    name: '彩虹屁生成器',
    link: 'https://chp.shadiao.app/',
    tags: '彩虹屁,夸夸,女朋友,开心',
    desc: '悄悄告诉各位男同学，这个网站一定要收好，如果哪天词穷了，女朋友又不开心了，打开它，保你逢凶化吉。'
  },

  {
    free: true,
    name: '方案通',
    link: 'http://file.heimaohui.com/',
    tags: '营销,策划,方案,创意,灵感',
    desc: '全网营销策划人必备的一款方案查询神器，为活动策划人提供策划创意灵感的平台，提供包括房地产，商场，汽车，消费品，政府等优质活动策划/营销传播方案。'
  },
  {
    free: true,
    name: '液态星球',
    link: 'https://alteredqualia.com/xg/examples/nebula_artefact.html',
    tags: '液态,星球,无用,无聊',
    desc: '之前推文介绍过，来自AlteRedQualia，液态星球的界面非常好看，我们可以看到网页上就是渐变背景与一颗球。'
  },
  {
    free: true,
    name: '中国大学MOOC（慕课）',
    link: 'https://www.icourse163.org/',
    tags: '完整,专业,靠谱,大学,自学,公开课',
    desc: '想学习完整、专业、靠谱课程的同学，首选慕课。'
  },
  {
    free: true,
    name: '全历史',
    link: 'https://www.allhistory.com/',
    tags: '历史,时光机,时间,世界,国家',
    desc: '它以时间轴为线索，分国家地域，几乎把整个世界历史的进程呈现在我面前。'
  },
  {
    free: true,
    name: '下厨房',
    link: 'http://www.xiachufang.com/',
    tags: '厨艺,百科',
    desc: '这个网站，是每个厨艺爱好者的「百科全书」，无脑首推。'
  },

  {
    free: true,
    name: 'Seeseed',
    link: 'https://www.seeseed.com/',
    tags: '图标,配色,字体,工具,设计,灵感',
    desc: '这是一个设计集合网站，花瓣、DBF等设计网站全都有。'
  },
  {
    free: false,
    name: '创客贴',
    link: 'https://www.chuangkit.com/',
    tags: '设计,灵感,模板',
    desc: '里面有超级多好看的模板供我挑选，让我这个毫无设计天赋的小白，也能三步做出不错的作品。如果是想追求图片美观，技术又不太过关的同学，创客贴绝对适合你。'
  },
  {
    free: true,
    name: 'pixabay',
    link: 'https://pixabay.com/zh/photos/',
    tags: '高清,无版权,图库,素材',
    desc: '一个超全且高清的无版权图库！。'
  },
  {
    free: true,
    name: 'wallhaven',
    link: 'https://wallhaven.cc/',
    tags: '电脑,壁纸',
    desc: '这里有很多动漫或者油画图片，有4K画质，分辨率特别高。想找电脑的壁纸的同学，不要错过，绝对有适合你口味的壁纸。'
  },
  {
    free: true,
    name: 'Pexels',
    link: 'https://www.pexels.com/zh-cn/',
    tags: '规格,尺寸,图片,无版权,图库,视频',
    desc: '在这里，你更容易找到多种规格尺寸的图片，比如手机的竖图，普通16：9的横图，都能轻松找到。'
  },

  {
    free: true,
    name: 'FootageCrate',
    link: 'https://footagecrate.com/',
    tags: '视频,特效,剪辑,设计',
    desc: '这里有超级多的视频特效源文件，对于需要剪辑视频，特效技术又一般的同学来说，简直就是必备神器。'
  },
  {
    free: true,
    name: 'videvo',
    link: 'https://www.videvo.net/',
    tags: '音频,素材,图库,视频',
    desc: '就像它的名字一样，处处体现对视频素材的专业性，对于常年需要剪辑视频的同学，我建议你常来逛逛。'
  },
  {
    free: true,
    name: 'Pixel Map 生成器',
    link: 'https://pixelmap.amcharts.com/',
    tags: '地图,生成器,世界,颜色',
    desc: '界面比上面的地图生成器多了一分高级感。在这里，你可以任意选择哪个地域，也可以任意调换颜色，通过它做出来的地图素材绝对亮眼。'
  },
  {
    free: true,
    name: '蓝光网',
    link: 'http://www.languang.co/',
    tags: '蓝光,电影',
    desc: '一个可以下载蓝光原盘的聚集地。'
  },
  {
    free: true,
    name: '秘迹搜索',
    link: 'https://mijisou.com/',
    tags: '规格,尺寸,图片,无版权,图库,视频',
    desc: '在这里，你更容易找到多种规格尺寸的图片，比如手机的竖图，普通16：9的横图，都能轻松找到。'
  },

  {
    free: true,
    name: '迅捷PDF转换器',
    link: 'https://app.xunjiepdf.com/',
    tags: '文档,转换,处理,翻译,合并,分割,水印',
    desc: '迅捷PDF转换器在线网站，里面集合了众多实用的在线工具，如文档转换、文档处理、文档翻译、音视频转化等等，可以满足我们工作、学习和生活中的大部分需求。'
  },
  {
    free: true,
    name: '狗屁不通文章生成器',
    link: 'https://suulnnka.github.io/BullshitGenerator/index.html',
    tags: '废话,报告,形式,文章,生成器',
    desc: '万字申请，废话报告，魔幻形式主义大作怎么写？朋友，狗屁不通文章生成器了解一下。'
  },
  {
    free: true,
    name: '唯美渐变生成器',
    link: 'https://webgradients.com/',
    tags: '渐变,配色',
    desc: '相信一些小伙伴们在做渐变的时候总是拉不出来好看的渐变，不用慌，唯美渐变生成器网站帮你一键搞定。'
  },
  {
    free: true,
    name: 'Png免扣图素材大全网站',
    link: 'http://pngimg.com/',
    tags: '免扣图,素材,图库',
    desc: '但是这个网站里有超多的已经扣好的png格式并且高质量素材，供大家免费使用下载！'
  },
  {
    free: true,
    name: '免费插画下载网站',
    link: 'https://undraw.co/illustrations',
    tags: '插画,图片,图库,颜色',
    desc: '这个网站最神奇的地方就是你可以选择自己喜欢的颜色，只需要点击网站的右上角色块选择器，选择一个自己喜欢的颜色，这样，插画里的内容就会自动变成你选择的颜色啦！真的超级方便有没有！'
  },
  
  
  {
    free: true,
    name: '模糊照片转高清网站',
    link: 'https://bigjpg.com/',
    tags: '模糊,照片,转高清,图片,无损,放大',
    desc: '它会将噪点和锯齿的部分进行补充，实现图片的无损放大。'
  },
  {
    free: true,
    name: 'unsplash',
    link: 'https://unsplash.com/',
    tags: '高清,唯美,图片,下载',
    desc: '这个网站上的图片高清唯美的我都想舔屏了！'
  },
  {
    free: true,
    name: '我要自学网',
    link: 'https://www.51zxw.net/',
    tags: '自学,影视,剪辑,设计,动画,程序,网页,会计',
    desc: '总而言之，只要你想要自学的，上面都有！大多数都是免费的，只有极少数课程需要支付一点点费用。'
  },
  {
    free: true,
    name: 'iLoveIMG',
    link: 'https://www.iloveimg.com/zh-cn',
    tags: '图片,压缩,滤镜,转换,水印,搞笑,创意',
    desc: '图片在线处理工具中，不可不介绍的当然是全能的图片编辑器'
  },
  {
    free: true,
    name: 'photopea',
    link: 'https://www.photopea.com/',
    tags: 'PSD,XCF,SKetch,XD,CAD,图片,编辑,处理',
    desc: '一个与 PS 电脑软件相似度非常高的在线 PS 网站，是一个免费并且非常强大的高级图像编辑器'
  },
  
  
  {
    free: true,
    name: 'wallhaven',
    link: 'https://wallhaven.cc/',
    tags: '壁纸,分类,图片,高清',
    desc: '优秀的壁纸网站,分类非常细致，而且满足绝大多数人的需求'
  },
  {
    free: true,
    name: '虚假截图助手',
    link: 'https://fakes.netlify.app/',
    tags: '虚假,截图,制作,编辑,检测',
    desc: '再来给大家安利一款实用到爆的网站，虚假截图助手，现在不会还有人觉得截图就百分百是正确的吧，现在懂点电脑技术的人都会知道，万物皆可改。'
  },
  {
    free: true,
    name: 'Toolfk在线工具箱',
    link: 'https://www.toolfk.com/',
    tags: '程序员,工具箱,代码,运行,平台,加密,转换,导航,开发',
    desc: 'Toolfk在线工具箱是一款程序员工具箱，包含几十个实用的工具。'
  },
  {
    free: true,
    name: '放射图片生成',
    link: 'https://wangyasai.github.io/Stars-Emmision/',
    tags: '放射,图片,生成,PPT,海报',
    desc: '这是一个放射图片的生成网站'
  },
  {
    free: true,
    name: '微软推出的模板网站',
    link: 'http://www.officeplus.cn/',
    tags: '微软,模板,PPT,WORD,EXCEL,精美,计划,日历,预算',
    desc: '你以为这就是一个单纯的模板网站？No，想的太简单了，微软在上面搜集了非常多有意思的模板，我们可以拿来制定减肥计划、每日计划安排、日历、预算表等等非常多有意思的模板。'
  },
];